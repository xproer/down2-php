﻿Vue.component('down2', {
    template: '<div class="down-panel"></div>',
    props: { config: Object },
    data: function () {
        return {
            app: null,
            pluginInited: false
        }
    },
    methods: {
        check_path: function () {
            if (this.app.Config["Folder"] == "") {
                this.app.setConfig();
                return false;
            }
            return true;
        },
        taskEmpty: function () {
            return this.app.queueWork.length < 1;
        },
        taskEnd: function () {
            this.app.stop_queue();
        }
    },
    mounted: function () {
        var _this = this;
        this.app = new DownloaderMgr({
            config: _this.config,
            event: {
                loadComplete: function () {
                    _this.pluginInited = true;
                    _this.$emit('load_complete');
                },
                sameFileExist: function (n) {
                    _this.$emit('same_file_exist', n);
                },
                unsetup: function (html) {
                    _this.$emit("unsetup", html);
                },
                fileAppend: function (o) {
                    _this.$emit("file_append", o);
                },
                selFolder: function (d) {
                    _this.$emit("folder_sel",d);
                },
                taskCreate:function(f){
                    _this.$emit("task_create",f);
                }
            },
            ui: { render: $(".down-panel") }
        });
    }
});