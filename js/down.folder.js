﻿//文件夹下载对象
function FolderDowner(fileLoc, mgr) {
    var _this = this;
    this.ui = { msg: null, process: null, percent: null, btn: { del: null, cancel: null, down: null, stop: null }, div: null};
    this.app = mgr.app;
    this.Manager = mgr;
    this.Config = mgr.Config;
    this.data = mgr.data;
    this.fields = $.extend(true,{}, mgr.Config.Fields,fileLoc.fields);//每一个对象自带一个fields幅本
    this.State = this.data.state.None;
    this.event = mgr.event;
    this.fileSvr = {
          id: ""//guid
        , uid: 0
        , nameLoc: ""//自定义文件名称
        , folderLoc: this.Config["Folder"]
        , pathLoc: ""
        , fileUrl: ""
        , lenLoc: 0
        , perLoc: "0%"
        , lenSvr: 0
        , sizeSvr: "0byte"
        , complete: false
        , fdTask: true
        , files:[]
    };
    $.extend(true,this.fileSvr, fileLoc);//覆盖配置

    this.hideBtns = function () {
        $.each(this.ui.btn, function (i, n) {
            $(n).hide();
        });
    };

    //方法-准备
    this.ready = function () {
        this.ui.btn.del.click(function () { _this.remove(); });
        this.ui.btn.stop.click(function () { _this.stop(); });
        this.ui.btn.down.click(function () { _this.down(); });
        this.ui.btn.cancel.click(function () { _this.remove(); });
        this.ui.btn.open.click(function () { _this.openPath(); });
        this.ui.btn.errInf.click(function(){_this.ui.errFiles.toggleClass("d-hide");});

        this.hideBtns();
        this.ui.btn.down.show();
        this.ui.btn.cancel.show();
        this.ui.name.text(this.fileSvr.nameLoc);
        this.ui.size.text(this.fileSvr.sizeSvr);
        this.ui.percent.text("(" + this.fileSvr.perLoc + ")");
        this.ui.process.css("width", this.fileSvr.perLoc);
        this.ui.percent.text("(" + this.fileSvr.perLoc + ")");
        this.ui.ico.file.hide();
        this.ui.ico.fd.show();
        this.Manager.queueWait.push(this.fileSvr.id);//添加到等待队列
        this.ui.msg.text("正在队列中等待...");
        this.State = this.data.state.Ready;
    };

    this.addQueue = function () {
        this.app.addFile(this.fileSvr);
    };

    //方法-开始下载
    this.down = function ()
    {
        this.Manager.add_work(this.fileSvr.id);
        this.hideBtns();
        this.ui.btn.stop.show();
        this.ui.btn.errInf.hide();
        this.ui.errFiles.html("");
        this.ui.msg.text("开始连接服务器...");
        this.State = this.data.state.Posting;
        this.app.downJson(this.fileSvr);//下载队列
    };

    //方法-停止传输
    this.stop = function () {
        this.hideBtns();
        //this.SvrUpdate();
        this.State = this.data.state.Stop;
        this.ui.msg.text("下载已停止");
        this.app.stopFile(this.fileSvr);
        this.Manager.del_work(this.fileSvr.id);//从工作队列中删除
    };

    this.remove = function () {
        this.app.stopFile(this.fileSvr);
        //从上传列表中删除
        this.ui.div.remove();
        this.Manager.remove_wait(this.fileSvr.id);
        this.Manager.remove_url(this.fileSvr.nameLoc);
        this.svr_delete();
    };

    this.open = function () {
        this.app.openFile(this.fileSvr);
    };

    this.openPath = function () {
        this.app.openPath({ pathLoc: this.fileSvr.pathLoc });
    };
    this.openChild = function (file) {
        this.app.openChild({ folder: this.fileSvr, "file": file });
    };

    //在出错，停止中调用
    this.svr_update = function () {
        if (!this.Config.DataBase) return;//

        var param = $.extend({}, this.fields, this.fileSvr, { time: new Date().getTime() });
        $.ajax({
            type: "GET"
            , dataType: 'jsonp'
            , jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
            , url: _this.Config["UrlUpdate"]
            , data: param
            , success: function (msg) { }
            , error: function (req, txt, err) { alert("更新下载信息失败！" + req.responseText); }
            , complete: function (req, sta) { req = null; }
        });
    };

    //在服务端创建一个数据，用于记录下载信息，一般在HttpDownloader_BeginDown中调用
    this.svr_create = function () {
        if (!this.Config.DataBase) return;//
        //已记录将不再记录
        var param = $.extend({}, this.fields, { file: encodeURIComponent(JSON.stringify(this.fileSvr)), time: new Date().getTime() });

        $.ajax({
            type: "GET"
            , dataType: 'jsonp'
            , jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
            , url: _this.Config["UrlCreate"]
            , data: param
            , success: function (msg) {
                if (msg.value == null) return;
                var json = JSON.parse(decodeURIComponent(msg.value));
                //文件已经下载完
                //if (_this.isComplete()) { _this.svr_delete(); }
            }
            , error: function (req, txt, err) { alert("创建信息失败！" + req.responseText); }
            , complete: function (req, sta) { req = null; }
        });
    };

    this.isComplete = function () { return this.State == this.data.state.Complete; };
    this.svr_delete = function () {
        if (!this.Config.DataBase) return;//
        var param = $.extend({}, this.fields, { id: this.fileSvr.id, time: new Date().getTime() });
        $.ajax({
            type: "GET"
            , dataType: 'jsonp'
            , jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
            , url: _this.Config["UrlDel"]
            , data: param
            , success: function (json) {}
            , error: function (req, txt, err) { alert("删除数据错误！" + req.responseText); }
            , complete: function (req, sta) { req = null; }
        });
    };

    this.query_process = function (json)
    {
        this.ui.size.text(json.sizeSvr);
        this.fileSvr.sizeSvr = json.sizeSvr;
        this.fileSvr.lenSvr = json.len;
        this.ui.percent.text("(" + json.percent + ")");
        var msg = [json.fileIndex , "/" , json.fileCount," 错误:",json.errCount];
        this.ui.msg.text( msg.join("") );
    };

    this.down_complete = function (json)
    {
        this.Manager.filesCmp.push(this);
        this.Manager.del_work(this.fileSvr.id);//从工作队列中删除
        this.Manager.remove_wait(this.fileSvr.id);
        this.Manager.remove_url(this.fileSvr.nameLoc);
        this.hideBtns();
        this.ui.btn.errInf.show();
        this.event.downComplete(this);//biz event
        this.ui.process.css("width", "100%");
        this.ui.percent.text("(100%)");

        if (typeof (json.errFiles) != "undefined") {
            var fileErrs = new Array();
            $.each(json.errFiles, function (i, n) {
                var tmp = "<li class=\"txt-ico\"><img title=\"{error}\" src=\"/js/error.png\"/>名称：{name} 大小：{size} <a class=\"btn-link\" href=\"{url}\" target=\"_blank\">打开链接</a></li>";
                tmp = tmp.replace(/{name}/gi, n.name);
                tmp = tmp.replace(/{url}/gi, n.url);
                tmp = tmp.replace(/{size}/gi, n.sizeSvr);
                tmp = tmp.replace(/{error}/gi, _this.data.errCode[n.errCode]);
                fileErrs.push(tmp);
            });
            this.ui.errFiles.html(fileErrs.join(""));
        }
        
        var msg = ["下载完毕", " 共:", json.fileCount,
            " 成功:", json.cmpCount,
            "(空:", json.emptyCount,
            ",重复:", json.repeatCount,
            ") 失败:", json.errCount,
            " 用时:", json.timeUse];
        this.ui.msg.text(msg.join(""));
        this.ui.btn.open.show();
        this.State = this.data.state.Complete;

        this.svr_delete();
        setTimeout(function () { _this.Manager.down_next(); }, 500);
    };

    this.down_process = function (json) {
        this.fileSvr.lenLoc = json.lenLoc;//保存进度
        this.fileSvr.perLoc = json.percent;
        this.ui.percent.text("(" + json.percent + ")");
        this.ui.process.css("width", json.percent);
        var msg = [json.index,"/",json.fileCount,
        " ",json.sizeLoc,
        " ", json.speed,
        " 剩余:", json.time,
        " 用时:", json.elapsed];
        this.ui.msg.text(msg.join(""));
    };

    this.down_begin = function (json) {
        var lenSvr = this.fileSvr.lenSvr;
        var filePart = this.Config["FilePart"];
        if (lenSvr > filePart ) {
            this.svr_create();
        }
    };

    this.down_error = function (json) {
        this.hideBtns();
        this.ui.btn.down.show();
        this.ui.btn.del.show();
        this.ui.btn.errInf.show();
        this.event.downError(this, json.code);//biz event
        this.ui.msg.text(this.data.errCode[json.code + ""]);

        var errors = new Array();
        var tmp = "<li class=\"m-t-xs\">文件：{count},\
        完成:{cmp}(空:{empty},重复:{repeat}),\
        错误:{err}(网络:{network},长路径:{lpath},空间不足:{full})\
        ,用时:{timeUse}</li>";
        tmp = tmp.replace(/{count}/gi, json.fileCount);
        tmp = tmp.replace(/{empty}/gi, json.emptyCount);
        tmp = tmp.replace(/{cmp}/gi, json.cmpCount);
        tmp = tmp.replace(/{err}/gi, json.errCount);
        tmp = tmp.replace(/{network}/gi, json.netCount);
        tmp = tmp.replace(/{repeat}/gi, json.repeatCount);
        tmp = tmp.replace(/{lpath}/gi, json.lpathCount);
        tmp = tmp.replace(/{full}/gi, json.fullCount);
        tmp = tmp.replace(/{timeUse}/gi, json.timeUse);
        errors.push(tmp);

        if (typeof (json.errFiles) != "undefined") {
            $.each(json.errFiles, function (i, n) {
                tmp = "\
                <li>\
                    <div class=\"txt-ico m-t-xs txt-limit-sm\">\
                        <a title=\"{name}\" class=\"txt-link\" href=\"{url}\" target=\"_blank\"><img name=\"err\"/>{name}</a>\
                    </div>\
                    <div style=\"margin:3px 0 0 0;\">大小：{size}</div>\
                    <div class=\"txt-limit-sm m-t-xs\" title=\"{path}\">路径：{path}</div>\
                    <div class=\"m-t-xs\">错误：{error}</div>\
                    <div class=\"m-t-xs\">错误码：{errCode}</div>\
                </li>";
                tmp = tmp.replace(/{name}/gi, n.name);
                tmp = tmp.replace(/{url}/gi, n.url);
                tmp = tmp.replace(/{size}/gi, n.sizeSvr);
                tmp = tmp.replace(/{error}/gi, _this.data.errCode[n.errCode]);
                tmp = tmp.replace(/{path}/gi, n.pathLoc);
                tmp = tmp.replace(/{errCode}/gi, n.errCodeNet);
                errors.push(tmp);
            });
        }
        this.ui.errFiles.html(errors.join(""));
        this.ui.errFiles.find("img[name='err']").each(function(n,i){
            $(this).attr("src",_this.Manager.data.ico.err);
        });
        this.State = this.data.state.Error;
        this.svr_update();
        this.Manager.del_work(this.fileSvr.id);//从工作队列中删除
        this.Manager.add_wait(this.fileSvr.id);
    };

    this.down_stoped = function (json) {
        this.hideBtns();
        this.ui.btn.down.show();
        this.ui.btn.del.show();
        this.Manager.del_work(this.fileSvr.id);//从工作队列中删除
        this.Manager.add_wait(this.fileSvr.id);
        this.ui.msg.text("下载已停止");
    };
}