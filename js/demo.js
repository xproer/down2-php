$(function () {
    var demos = [
        {
            name: "示例：",
            pages: [
                {
                    name: "清空数据库",
                    url: "db/clear.php"
                },
                {
                    name: "单个URL下载",
                    url: "index.htm"
                },
                {
                    name: "URL批量下载",
                    url: "urls.htm"
                },
                {
                    name: "自定义文件名称",
                    url: "diy-name.htm"
                },
                {
                    name: "json格式下载",
                    url: "json.htm"
                },
                {
                    name: "json多层级下载",
                    url: "jsons.htm"
                },
                {
                    name: "下载后自动运行",
                    url: "down-run.htm"
                },
                {
                    name: "自定义下载",
                    url: "cusdata.htm"
                },
                {
                    name: "自定义下载多个文件",
                    url: "cus-files.htm"
                },
                {
                    name: "大文件下载测试",
                    url: "bigfile.htm"
                },
                {
                    name: "vue示例",
                    url: "vue.htm"
                }
            ]
        },
        {
            name: "调试：",
            pages: [
                {
                    name: "单文件下载",
                    url: "debug/file.htm"
                },
                {
                    name: "空文件下载",
                    url: "debug/empty.htm"
                },
                {
                    name: "1个空文件测试",
                    url: "debug/err-fd-1.htm"
                },
                {
                    name: "错误文件下载",
                    url: "debug/error.htm"
                },
                {
                    name: "名称长度限制",
                    url: "debug/limit.htm"
                },
                {
                    name: "正常重复数据下载",
                    url: "debug/nor-repeat.htm"
                },
                {
                    name: "错误重复数据下载",
                    url: "debug/err-repeat.htm"
                },
                {
                    name: "正常数据下载",
                    url: "debug/normal.htm"
                },
                {
                    name: "正常多条数据下载",
                    url: "debug/normals.htm"
                },
                {
                    name: "重复数据下载",
                    url: "debug/repeat.htm"
                }
            ]
        }
    ];

    //url=>res/
    //http://localhost:8888/filemgr/res/up6/up6.js=>
    this.getJsDir = function () {
        var js = document.scripts;
        var jsPath;
        for (var i = 0; i < js.length; i++) {
            if (js[i].src.indexOf("js/demo.js") > -1) {
                jsPath = js[i].src.substring(0, js[i].src.indexOf("js/demo.js"));
            }
        }
        return jsPath;
    };
    //http://localhost/res/down2/
    var root = this.getJsDir();
    $(demos).each(function (i, n) {
        var tmps = ["<ul>", "<li><b>" + n.name + "</b></li>"];
        $(n.pages).each(function (i, n) {
            tmps.push("<li><a target='_blank' href='" + root + n.url + "'>" + n.name + "</a></li>");
        });
        tmps.push("</ul>");
        $("#demos").append(tmps.join(""));
    });
});