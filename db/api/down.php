<?php
ob_start();
header('Content-type: text/html;charset=utf-8');
/*
	更新记录：
		2017-12-11 更新
*/
$nameSvr = "Dreamweaver_12_LS3.zip.td";
$pathSvr = "d:/Dreamweaver_12_LS3.zip.td";
	//First, see if the file exists
	if (!is_file($pathSvr)) 
	{
		header("HTTP/1.1 500 file not found");
		die("<b>404 File not found!</b>"); 
	}

	//Gather relevent info about file
	$len = filesize($pathSvr);		

	//Begin writing headers
	header("Cache-Control:");
	header("Cache-Control: public");
	header("Pragma: No-cache");
	header("Expires: 0");	
	header("Content-Type: application/octet-stream;charset:utf-8");

	$ie = isset( $_SERVER['HTTP_USER_AGENT'] );
	if( $ie) $ie = strstr($_SERVER['HTTP_USER_AGENT'], "MSIE");
	
	if ( $ie)
	{
		# workaround for IE filename bug with multiple periods / multiple dots in filename
		# that adds square brackets to filename - eg. setup.abc.exe becomes setup[1].abc.exe
		$iefilename = preg_replace('/\./', '%2e', $nameSvr, substr_count($nameSvr, '.') - 1);
		header("Content-Disposition: attachment; filename=$iefilename");
	}
	else 
	{
		header("Content-Disposition: attachment; filename=$nameSvr");
	}
	header("Accept-Ranges: bytes");

	$fileLen = filesize($pathSvr);
	$blockSize = $fileLen;
	$blockBegin = 0;
	$blockEnd = 0;
			
	if(isset($_SERVER['HTTP_RANGE'])) 
	{
		list($a, $range) = explode("=",$_SERVER['HTTP_RANGE']);//$range=500-1000
		list($range_begin,$range_end) = explode("-",$range);
		
		//-1
		if(strlen($range_begin)<0)
		{
			$blockSize = $range_end;
			$blockEnd = $fileLen - 1;
			$blockBegin = $fileLen - $blockSize;			
		}//1-
		else if( strlen($range_end) <0)
		{
			$blockSize = $fileLen - $range_begin;
			$blockBegin = $range_begin;
			$blockEnd = ((int)$blockBegin + (int)$blockSize) - 1;			
		}//1-1
		else 
		{
			$blockSize = ((int)$range_end - (int)$range_begin) + 1;
			$blockBegin = $range_begin;
			$blockEnd = $range_end;			
		}
		header("HTTP/1.1 206 Partial Content");
		header("Content-Length: $blockSize");
		header("Content-Range: bytes $blockBegin-$blockEnd/$fileLen");
	}
	
	//open the file
	$fp = fopen($pathSvr,"rb");	
	//seek to start of missing part
	fseek($fp,$blockBegin);
	
	$size = 1048576;
	//start buffered download
	while($blockSize > 0){
		if( $blockSize < $size) $size = $blockSize;
		//reset time limit for big files
		set_time_limit(0);
		print(fread($fp,$size));//每次读取1MB
		flush();
		ob_flush();
		$blockSize -= $size;
	}
	fclose($fp);
	exit;
?>