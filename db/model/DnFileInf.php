<?php
/*
	说明：
		1.在调用此函数前不能有任何输出操作。比如 echo print
		
	更新记录：
		2014-08-12 创建
*/
class DnFileInf extends sql\DataBaseAttribute
{	
    var $id="";
    var $uid = 0;
    var $mac="";
	var $nameLoc="";
    var $pathLoc="";
    var $fileUrl="";
    var $lenLoc = 0;
    var $lenSvr = 0;	
	var $sizeSvr = "0byte";	
	var $perLoc = "0%";
	var $fdTask = false;
	
	function __construct()
	{
		//绑定数据表字段
		$this->bindColumns(array(
			"id" => new sql\SqlField("f_id","string",32,true),
			"uid" => new sql\SqlField("f_uid","int",4),
			"mac" => new sql\SqlField("f_mac","string",50),
			"nameLoc" => new sql\SqlField("f_nameLoc","string",255),
			"pathLoc" => new sql\SqlField("f_pathLoc","string",255),
			"fileUrl" => new sql\SqlField("f_fileUrl","string",255),
			"lenLoc" => new sql\SqlField("f_lenLoc","int",8),
			"lenSvr" => new sql\SqlField("f_lenSvr","int",8),
			"sizeSvr" => new sql\SqlField("f_sizeSvr","string",10),
			"perLoc" => new sql\SqlField("f_perLoc","string",6),
			"fdTask" => new sql\SqlField("f_fdTask","bool",1)
		));
	}
}
?>