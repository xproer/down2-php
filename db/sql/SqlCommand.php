<?php
namespace sql;

use DbHelper;

/**
 * 用法：
 * SqlCommand::build()->sql("file")
 * ->execReader();
 */
class SqlCommand{
    private $m_sql="";

    public static function build()
    {
        return new SqlCommand();
    }

    function __construct()
    {
    }

    function sql($s){
        $this->m_sql = $s;
        return $this;
    }

    /**
     * 返回受影响的行数
     */
    function exec(){
        $con = DbHelper::build()->GetConUtf8();
        return $con->exec($this->m_sql);
    }

    function execInt(){}
    function execBool(){}
    function execStmt(){
        $stmt = DbHelper::build()->prepare_utf8($this->m_sql);
        return $stmt;
    }
}
?>