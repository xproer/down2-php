<?php
namespace sql;
class SqlField{
    public $name="";//数据表字段名称
    public $type="";//字段类型,int,string,bool
    public $len=1;//字段长度
    public $primary=false;
    
    function __construct($n,$t,$l,$pk=false)
    {
        $this->name = $n;
        $this->type = $t;
        $this->len = $l;
        $this->primary=$pk;
    }

    function pdoParam()
    {
        return ":".$this->name;
    }

    function pdoType()
    {
        if($this->type=="int") return \PDO::PARAM_INT;
        if($this->type=="bool") return \PDO::PARAM_BOOL;
        if($this->type=="string") return \PDO::PARAM_STR;
    }
    
    function pdoValue($v)
    {
        if($this->type=="int") return intval($v);
        if($this->type=="bool") return (bool)$v;
        if($this->type=="string") return $v;
    }
}
?>