<?php
namespace sql;
class SqlWhere{
    //字段表：name,SqlValue
    public $m_vals=array();//变量值
    public $m_sqls=array();//sql语句列表
    
    public static function build()
    {
        return new SqlWhere();
    }

    function __construct()
    {
    }

    function eq($n,$v)
    {
        //:f_name=name
        $this->m_vals[$n]=SqlValue::build($n,$v);
        return $this;
    }

    function sql($v)
    {
        //id=1
        $this->m_sqls[]=$v;
        return $this;
    }

    function toSql()
    {
        $sql=" where ";
        $vals=array();
        foreach($this->m_vals as $n => $v)
        {
            $vals[]=$n."=".$v->pdoParam();
        }
        for($i=0,$l=count($this->m_sqls);$i<$l;++$i)
        {
            $vals[] = $this->m_sqls[$i];
        }
        if(count($vals)==0) return "";
        return $sql . join(" and ",$vals);
    }

    /**
     * $index 变量起始索引
     */
    function bind(&$stmt/**PDOStatement*/)
    {
        foreach($this->m_vals as $n => $val/**SqlValue */)
        {
            $stmt->bindValue($val->pdoParam(),
            $val->m_val,
            $val->pdoType());
        }
        return $this;
    }
}
?>