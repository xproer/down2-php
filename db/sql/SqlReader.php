<?php
namespace sql;
class SqlReader{
    private $m_stmt=null;

    public static function build($stmt)
    {
        return new SqlReader($stmt);
    }

    function __construct($stmt/**PDOStatement */)
    {
        $this->m_stmt=$stmt;
    }

    function read()
    {
        return $this->m_stmt->fetch();
    }
}
?>