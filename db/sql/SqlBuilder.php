<?php
namespace sql;
/**
 * 用法：
 * 1.指定列
 * SqlBuilder::build()->table("file")
 * ->columns("f_id,f_name")
 * ->where()
 * ->select();
 * 
 * 1.通过DataBaseAttribute
 * SqlBuilder::build()->table("file")
 * ->dataAttr(attr)
 * ->where()
 * ->select();
 */
class SqlBuilder{
    public $m_table="";
    public $m_columns="";
    public $m_pars="";//:a,:b,:c
    public $m_dataAttr=null;
    public $m_where=null;

    public static function build()
    {
        return new SqlBuilder();
    }

    function __construct()
    {
    }

    function table($t){
        $this->m_table=$t;
        return $this;
    }

    function columns($cols)
    {
        $this->m_columns=$cols;
        return $this;
    }

    function dataAttr(&$dba/**DataBaseAttribute */)
    {
        $this->m_columns = $dba->getColumnNames();
        $this->m_pars = $dba->getColumnPars();
        $this->m_dataAttr = $dba;
        return $this;
    }

    function select()
    {
        //select * from table
        $sql = "select ".
        $this->m_columns.
        " from ".
        $this->m_table.
        " ";

        if(!is_null($this->m_where))
        {
            $sql = $sql . $this->m_where->toSql();
        }
        return $sql;
    }

    function insert()
    {
        //insert into test (userid) values(:i)
        $sql = "insert into ".
        $this->m_table.
        " (".
        $this->m_columns.
        ") values(".
        $this->m_pars.
        ")"
        ;

        return $sql;   
    }

    function where($w/**SqlWhere */)
    {
        $this->m_where = $w;
        return $this;
    }

    function update($cols="")
    {
        //UPDATE EMPLOYEE SET FNAME=’HAIWER’ WHERE EMP_ID=’ VPA30890F’
        $sql = "update ".
        $this->m_table.
        " set ".
        $this->m_dataAttr->getColumnSets($cols).
        " ".
        $this->m_where->toSql()
        ;

        return $sql;
    }

    function delete()
    {
        $sql = "delete from ".
        $this->m_table.
        " ".
        $this->m_where->toSql();
        return $sql;
    }
}
?>