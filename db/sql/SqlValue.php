<?php
namespace sql;
class SqlValue{
    public $m_name;//数据库字段名称
    public $m_val;//字段值
    //integer,boolean,string,double
    public $m_type;//类型

    public static function build($n,$v)
    {
        return new SqlValue($n,$v);
    }

    function __construct($n,$v)
    {
        $this->m_name = $n;
        $this->m_val = $v;
        $this->m_type = gettype($v);
    }

    function pdoType()
    {
        if($this->m_type=="integer") return \PDO::PARAM_INT;
        if($this->m_type=="boolean") return \PDO::PARAM_BOOL;
        if($this->m_type=="string") return \PDO::PARAM_STR;
    }

    function pdoParam()
    {
        return ":".$this->m_name;
    }
}
?>