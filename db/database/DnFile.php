<?php
/*
	说明：
		1.在调用此函数前不能有任何输出操作。比如 echo print
		
	更新记录：
		2023-07-09 优化
		2014-09-01 创建
*/

use sql\SqlTable;

class DnFile 
{
	function __construct() 
	{
	}

	function table(){
		return sql\SqlTable::build()->table("down_files");
	}

    function Add(&$inf/**DnFileInf */)
    {
		$this->table()->insert($inf);
    }

    /// <summary>
    /// 删除文件
    /// </summary>
    /// <param name="fid"></param>
    function Delete($fid,$uid)
    {
		$w = sql\SqlWhere::build()->eq("f_id",$fid)
		->eq("f_uid",$uid);

		$this->table()->where($w)->delete();
    }

    /**
     * 更新文件进度信息
     * @param fid
     * @param uid
     * @param mac
     * @param lenLoc
     */
    function update($fid,$uid,$lenLoc,$perLoc,$nameLoc,$pathLoc,$lenSvr,$sizeSvr)
    {
		$inf = new DnFileInf();
		$inf->lenLoc = $lenLoc;
		$inf->perLoc = $perLoc;
		$inf->lenSvr = $lenSvr;
		$inf->sizeSvr = $sizeSvr;

		$this->table()
		->dataAttr($inf)
		->where(sql\SqlWhere::build()->eq("f_id",$fid)->eq("f_uid",$uid))
		->update("f_lenLoc,f_lenSvr,f_sizeSvr,f_perLoc");
    }
	
	function clear()
	{
		$this->table()->clear();
	}

    /// <summary>
    /// 获取所有未完成的文件列表
    /// </summary>
    /// <returns></returns>
    function GetAll($uid)
    {
		$inf = new DnFileInf();
		$files = $this->table()
		->where(sql\SqlWhere::build()->eq("f_uid",$uid))
		->reads($inf);
	    return json_encode($files);
	}
}