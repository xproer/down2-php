- down2官网：http://www.ncmem.com
- 控件下载：http://www.ncmem.com/webapp/down2/index.aspx
- 示例下载：http://www.ncmem.com/webapp/down2/versions.aspx
- 在线演示：http://www.ncmem.com/products/down2/index.html
- 在线文档：http://www.ncmem.com/doc/view.aspx?id=9e76fa9ce739455991acf27391c63ee6

#### 介绍
泽优大文件下载控件（down2）是由荆门泽优软件有限公司开发的一个面向政府核心部门和关键企业的高性能数据安全产品。
down2能够广泛适用于OA办公系统，电子政务系统，党政系统，军工系统，网盘系统，云盘系统，招投标系统，文件管理系统，资源管理系统。
目前down2已经完成全平台覆盖，支持信创环境国产CPU（海光,兆芯,鲲鹏,飞腾,龙芯），国产操作系统（中标麒麟，银河麒麟，统信UOS，深度，优麒麟，Ubuntu,CentOS,Linux），国产数据库（达梦数据库，人大金仓），能够帮助企业产品完成国产化的平滑迁移升级和政务信息化的顺利落地。

#### 安装控件

- Windows: http://www.ncmem.com/doc/view.aspx?id=1dda2eef4dc94ce0bdff05df9b126a59
- macOS: http://www.ncmem.com/doc/view.aspx?id=15d5620f1b5746b495df19b72b45bee2
- Linux-deb: http://www.ncmem.com/doc/view.aspx?id=4848c005740447a6b3ec90b50d6a0600
- Linux-rpm: http://www.ncmem.com/doc/view.aspx?id=916ff54382f2436686cb2c58fdb689b0

#### 成功案例

- 北京银联信科技股份有限公司
- 优慕课在线教育科技（北京）有限责任公司
- 西安工业大学
- 西安恒谦教育科技股份有限公司
- 西安德雅通科技有限公司
- 国家气象中心
- 国开泛在（北京）教育科技有限公司
- 北京大唐融合通信技术有限公司
- 北京思路创新科技有限公司
- 北京兴油工程项目管理有限公司
- 北京海泰方圆科技股份有限公司
